from django.db import models

# Create your models here.


class Usager(models.Model):
    nom = models.CharField(max_length=50)
    prenom = models.CharField(max_length=50)
    ddn = models.DateField()
    adresse = models.CharField(max_length=100)


class Plat(models.Model):
    nom = models.CharField(max_length=50)
    nombre_portion = models.IntegerField(default=1)
    prix = models.DecimalField(max_digits=6, decimal_places=2)


class Livreur(models.Model):
    nom = models.CharField(max_length=50)
    marque_voiture = models.CharField(max_length=50)
    positionGPSX = models.FloatField()
    positionGPSY = models.FloatField()


class Commande(models.Model):
    date_expiration = models.DateTimeField()
    date_expiration = models.DateTimeField()
    livreur = models.ForeignKey(Livreur, on_delete=models.CASCADE)
    usager = models.ForeignKey(Usager, on_delete=models.CASCADE)
    plat = models.ManyToManyField(Plat)
