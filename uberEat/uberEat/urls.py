from django.contrib import admin
from django.urls import include, path
urlpatterns = [
 path('priseDeCommande/', include('priseDeCommande.urls')),

 path('admin/', admin.site.urls),
]
